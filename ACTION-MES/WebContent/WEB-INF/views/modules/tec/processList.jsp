<%--
  Created by IntelliJ IDEA.
  User: 10720
  Date: 2019/9/7
  Time: 9:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>工艺维护</title>
    <script type="text/javascript">
        function page(n,s){
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
<!--1.tab-->
<ul class="nav nav-tabs">
    <li class="active">
        <a href="${ctx}/tec/process/">工序列表</a>
    </li>
    <li>
        <a href="${ctx}/tec/process/form">工序添加</a>
    </li>
</ul>
<!--2.查询-->
<form:form id="searchForm" method="post" action="${ctx}/tec/process" modelAttribute="process" class="breadcrumb form-search">
    <input id="pageNo" name="pageNo" type="hidden" value=${page.pageNo}/>
    <input id="pageSize" name="pageSize" type="hidden" value=${page.pageSize}/>
    <div class="controls">
        <lable>工序名称：</lable>
        <form:input path="proCode" maxlength="50" class="input-mdeium" htmlEscape="false"/>
        <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary"/>
    </div>
</form:form>
<sys:message content="${message}"/>
<!--3.列表-->
<table id="contentTable" class="table table-striped table-bordered table-condensed">
    <thead>
    <th>工序编号</th>
    <th>工序名称</th>
    <th>描述</th>
    <th>操作</th>
    </thead>
    <tbody>
    <c:forEach items="${page.list}" var="process">
        <tr>
            <td>${process.proCode}</td>
            <td>${process.proName}</td>
            <td>${process.proDesc}</td>
            <td>
                <a href="${ctx}/tec/process/form?id=${process.id}">修改</a>
                <a href="${ctx}/tec/process/delete?id=${process.id}" onclick="return confirmx('确认要删除该工序信息吗？',this.href)">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<!--4.分页-->
<div class="pagination">${page}</div>
</body>
</html>
