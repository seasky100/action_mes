<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>Bom管理</title>
    <script type="text/javascript">
        function page(n,s){
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
<!--1.tab-->
<ul class="nav nav-tabs">
    <li class="active">
        <a href="${ctx}/trace/traceWorkOrder">原材料追溯列表</a>
    </li>
</ul>
<!--2.查询-->
<form:form id="searchForm" method="post" action="${ctx}/trace/traceWorkOrder/" modelAttribute="traceWorkOrder" class="breadcrumb form-search">
    <input id="pageNo" name="pageNo" type="hidden" value=${page.pageNo}/>
    <input id="pageSize" name="pageSize" type="hidden" value=${page.pageSize}/>
    <div class="controls">
        <lable>工单号：</lable>
        <form:input path="orderCode" maxlength="50" class="input-mdeium" htmlEscape="false"/>
        <input id="btnSubmit" type="submit" value="查询" class="btn btn-primay"/>
    </div>
</form:form>
<!--3.列表-->
<table id="contentTable" class="table table-striped table-bordered table-condensed">
    <thead>
    <th>工单号</th>
    <th>订单号</th>
    <th>订购商</th>
    <th>产品名称</th>
    <th>数量</th>
    <th>单位</th>
    <th>订购日期</th>
    <th>销售人</th>
    <th>状态</th>
    </thead>
    <tbody>
    <c:forEach items="${page.list}" var="traceworkorder">
        <tr>
            <td>${traceworkorder.workOrder.orderCode}</td>
            <td>${traceworkorder.orderNo}</td>
            <td>${traceworkorder.indentor}</td>
            <td>${traceworkorder.product.productName}</td>
            <td>${traceworkorder.quantity}</td>
            <td>${traceworkorder.unit}</td>
            <td>${traceworkorder.orderDate}</td>
            <td>${traceworkorder.salesman}</td>
            <td>${traceworkorder.status}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<!--4.分页-->
<div class="pagination">${page}</div>
</body>
</html>