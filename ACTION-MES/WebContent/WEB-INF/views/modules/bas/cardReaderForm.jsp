<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <title>读卡器管理</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler:function (form) {
                    loading('正在提交，请稍后.....');
                    form.submit();
                },
                errorContainer:"#messageBox",
                errorPlacement:function (error,element) {
                    $("#messageBox").text("输入有误，请更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    }else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
<ul class="nav nav-tabs">
    <li><a href="${ctx}/bas/cardReader/">读卡器列表</a></li>
    <li class="active"><a href="">读卡器${not empty cardReader.id?'修改':'添加'}</a></li>
</ul>
<br/>
<form:form id="inputForm" method="post" action="${ctx}/bas/cardReader/save" modelAttribute="cardReader" class="form-horizontal">
    <form:hidden path="id"/>
    <div class="control-group">
        <label class="control-label">设备编号：</label>
        <div class="controls">
            <form:input path="qrCode" htmlEscape="false" maxlength="50" class="required"/>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">设备类型：</label>
        <div class="controls">
            <form:select path="type" id="type" class="input-medium required">
                <form:option value="" label=""/>
                <form:options items="${fns:getDictList('equip_type')}" itemLable="label" itemValue="value" htmlEscape="false"/>
            </form:select>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">设备规格：</label>
        <div class="controls">
            <form:select path="spec" id="type" class="input-medium required">
                <form:option value="" label=""/>
                <form:options items="${fns:getDictList('equip_spec')}" itemLable="label" itemValue="value" htmlEscape="false"/>
            </form:select>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">所属工站:</label>
        <div class="controls">
            <form:select path="workStationInfos.id" id="sId" class="input-medium required">
                <form:option value="" label=""/>
                <form:options items="${workStationInfosList}" itemLabel="stationName" itemValue="id" htmlEscape="false"/>
            </form:select>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">所属工位:</label>
        <div class="controls">
            <form:select path="workCell.id" id="cId" class="input-medium required">
                <form:option value="" label=""/>
                <form:options items="${workCellList}" itemLabel="cellName" itemValue="id" htmlEscape="false"/>
            </form:select>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">读取速度：</label>
        <div class="controls">
            <form:input path="readerSpeed" tmlEscape="false" maxlength="50" class="required"/>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">打卡间隔：</label>
        <div class="controls">
            <form:input path="timeInterval" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">感应距离：</label>
        <div class="controls">
            <form:input path="distance" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">读卡类型：</label>
        <div class="controls">
            <form:input path="readerType" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">读卡频率：</label>
        <div class="controls">
            <form:input path="frequency" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">供应商：</label>
        <div class="controls">
            <form:input path="supplier" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">生产商：</label>
        <div class="controls">
            <form:input path="manufacturer" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">出厂编号：</label>
        <div class="controls">
            <form:input path="factoryNumber" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">用途：</label>
        <div class="controls">
            <form:input path="purpose" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">采购日期：</label>
        <div class="controls">
            <form:input path="buyDate" tmlEscape="false" maxlength="50" class="input-larg required Wdate"
                        onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false})"/>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">资产负责人：</label>
        <div class="controls">
            <form:input path="person" tmlEscape="false" maxlength="50" class="required"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">所属部门:</label>
        <div class="controls">
            <sys:treeselect id="organization" name="organization.id" value="${cardReader.organization.id}" labelName="organization.name"
                            labelValue="${cardReader.organization.name}"
                            title="机构" url="/sys/office/treeData"
                            cssClass="input-large" allowClear="true"></sys:treeselect>
            <span class="help-inline"><font color="red">*</font></span>
        </div>
    </div>
    <div class="form-actions">
        <input id="btnSubmit" class="btn btn-primary" type="submit" value="保存"/>&nbsp;
        <input id="btnCancel" class="btn" type="button" value="返回" onclick="history.go(-1)"/>
    </div>
</form:form>
</body>
</html>

