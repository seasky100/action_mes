<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>工厂管理</title>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <%--1.tab头部--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/bas/factory/list/">工厂列表</a></li>
        <li class="active"><a href="">工厂${not empty factory.id?'修改':'添加'}</a> </li>
    </ul>
    <br/>
    <%--2.表单部分--%>
    <form:form id="inputForm" method="post" action="${ctx}/bas/factory/save" modelAttribute="factory" class="form-horizontal">
    <form:hidden path="id"/>
    <sys:message content="${message}"/><div class="control-group">
        <div class="control-group">
        <label class="control-label">所属企业：</label>
            <div class="controls">
                <form:select path="enterprise.id" id="enterId" class="input-medium required">
                    <form:option value="" label=""/>
                    <form:options items="${enterpriseList}" itemLabel="enterName" itemValue="id" htmlEscape="false"/>
                </form:select><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">工厂名称：</label>
            <div class="controls">
                <form:input path="factoryName" htmlEscape="false" maxlength="50" class="required"/><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">建筑日期：</label>
            <div class="controls">
                <form:input path="factoryBuildDate" htmlEscape="false" maxlength="50" class="required"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">地址：</label>
            <div class="controls">
                <form:input path="factoryAddress" htmlEscape="false" maxlength="50" class="required"/><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">电话：</label>
            <div class="controls">
                <form:input path="factoryPhone" htmlEscape="false" maxlength="50" class="required"/><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">邮政编码：</label>
            <div class="controls">
                <form:input path="factoryECode" htmlEscape="false" maxlength="50" class="required"/><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">建筑面积：</label>
            <div class="controls">
                <form:textarea path="factoryBuildM" htmlEscape="false" maxlength="50" />
            </div>
        </div>
        <div class="form-actions">
            <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
            <input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
        </div>
    </form:form>
</body>
</html>
