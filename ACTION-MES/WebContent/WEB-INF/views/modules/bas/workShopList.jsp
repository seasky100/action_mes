<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <script type="text/javascript">
        function page(n,s) {
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("searchForm").submit();
        }
    </script>
    <title>车间管理</title>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li class="active"><a href="${ctx}/bas/workShop/list">车间列表</a></li>
        <li><a href="${ctx}/bas/workShop/form">车间添加</a> </li>
    </ul>
    <%--查询部分--%>
    <form:form id="searchForm" method="post" action="${ctx}/bas/workShop/" modelAttribute="workShop" class="breadcrumb form-search">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}">
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}">
        <label>车间名称</label>
        <form:input path="shopName" maxlength="50" class="input-medium" htmlEscape="false"/>
        <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
    </form:form>
    <sys:message content="${message}"/>
    <%--列表部分--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <td>车间名称</td>
            <td>所属工厂</td>
            <td>车间编码</td>
            <td>车间负责人</td>
            <td>车间描述</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${page.list}" var="workShop">
            <tr>
                <td>${workShop.shopName}</td>
                <td>${workShop.factory.factoryName}</td>
                <td>${workShop.shopNo}</td>
                <td>${workShop.master}</td>
                <td>${workShop.description}</td>
                <td>
                    <a href="${ctx}/bas/workShop/form?id=${workShop.id}">修改</a>
                    <a href="${ctx}/bas/workShop/delete?id=${workShop.id}" onclick="return confirm('确认要删除该车间吗？',this.href)">删除</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--分页部分--%>
    <div class="pagination">
        ${page}
    </div>
</body>
</html>
