<%--
  Created by IntelliJ IDEA.
  User: 97674
  Date: 2019/8/31
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <meta name="decorator" content="default">
    <title>工位与员工信息管理</title>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler:function (form) {
                    loading('正在提交，请稍后...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function (error,element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    }
                    else{
                        error.insertAfter(element);
                    }
                }
            })
        });
    </script>
</head>
<body>
<!--1.tab头部-->
<ul class="nav nav-tabs">
    <li><a href="${ctx}/bas/cellEmployee">工位与员工关系列表</a></li>
    <li class="active"><a href="">工位与员工关系${not empty cellEmployee.id?'修改':'添加'}</a></li>
</ul>
<!--2.表单-->
<form:form id="inputForm" method="post" action="${ctx}/bas/cellEmployee/save" modelAttribute="cellEmployee"
           class="form-horizontal">
    <form:hidden path="id"/>
    <sys:message content="${message}"/>
    <div class="control-group">
        <label class="control-label">员工：</label>
        <div class="controls">
            <form:select path="employee.id" id="eId" class="input-medium required">
                <form:option value="" label=""/>
                <form:options items="${employeeList}" itemLabel="employeeName" itemValue="id" htmlEscape="false"/>
            </form:select>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">所属工位：</label>
        <div class="controls">
            <form:select path="workCell.id" id="cId" class="input-medium required">
                <form:option value="" label=""/>
                <form:options items="${workCellList}" itemLabel="cellName" itemValue="id" htmlEscape="false"/>
            </form:select>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="form-actions">
        <input id="btnSubmit" class="btn btn-primary" type="submit" value="保存">&nbsp;
        <input id="btnCancel" class="btn btn-primary" value="返回" onclick="history.go(-1)">
    </div>
</form:form>
</body>
</html>
