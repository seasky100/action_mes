<%--
  Created by IntelliJ IDEA.
  User: z&w
  Date: 2019/9/7
  Time: 上午 10:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%><html>
<head>
    <meta name="decorator" content="default">
    <%--    装饰器模板--%>
    <title>订单信息</title>
    <script type="text/javascript">
        function page(n,s) {
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
    <%--1.tab--%>
    <ul class="nav nav-tabs">
        <li class="active"><a href="${ctx}/bas/order/">订单列表</a></li>
    </ul>

    <%--2.列表--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <th>订单号</th>
        <th>订购商</th>
        <th>产品名称</th>
        <th>数量</th>
        <th>单位</th>
        <th>订购日期</th>
        <th>销售人</th>
        <th>状态</th>
        <th>操作</th>
        </thead>
        <tbody>
        <c:forEach items="${page.list}" var="order">
            <tr>
                <td>${order.orderNo}</td>
                <td>${order.indentor}</td>
                <td>${order.product.productName}</td>
                <td>${order.quantity}</td>
                <td>${fns:getDictLabel(order.unit,'unit','')}</td>
                <td>${order.orderDate}</td>
                <td>${order.salesman}</td>
                <td>${fns:getDictLabel(order.status,'order_status','')}</td>
                <td>
                    <a href="${ctx}/bas/order/doWork?id=${order.id}">下工单</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <sys:message content="${message}"/>
    <%--3.分页--%>
    <div class="pagination">${page}</div>
</body>
</html>
