<%--
  Created by IntelliJ IDEA.
  User: z&w
  Date: 2019/9/10
  Time: 上午 10:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%><html>
<head>
    <meta name="decorator" content="default">
    <%--    装饰器模板--%>
    <title>工单管理</title>
    <script type="text/javascript">
        function page(n,s) {
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("searchForm").submit();
        }
    </script>
</head>
<body>
    <%--1.tab--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/bas/workOrder/">工单列表</a></li>
        <li class="active"><a href="">工艺流程</a></li>
    </ul>
    <!--2.查询-->
    <form:form id="searchForm" modelAttribute="workOrder" class="breadcrumb form-select">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
        <lable>工单号：</lable>
        <form:input path="orderCode" maxlength="50" class="input-mdeium" htmlEscape="false" readonly="true"/>
        <lable>产品：</lable>
        <form:input path="product.productName" maxlength="50" class="input-mdeium" htmlEscape="false" readonly="true"/>
        <lable>工艺流程名称：</lable>
        <form:input path="" value="${product.flow.flowName}" maxlength="50" class="input-mdeium" htmlEscape="false" readonly="true"/>
    </form:form>
    <!--3.列表-->
    <label style="padding-left: 10px;padding-bottom: 5px">工艺流程详情：</label>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <th>工序编码</th>
        <th>工序名称</th>
        <th>工序表述</th>
        </thead>
        <tbody>
        <c:forEach items="${page.list}" var="flowProcess">
            <tr>
                <td>${flowProcess.process.proCode}</td>
                <td>${flowProcess.process.proName}</td>
                <td>${flowProcess.process.proDesc}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</body>
</html>
