<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>切片工站绩效</title>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出用户数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/kpi/chopping/export");
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
        });
        function page(n,s) {
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/kpi/chopping/list");
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li class="active"><a href="${ctx}/kpi/chopping/list">切片工站绩效</a></li>
        <li><a href="${ctx}/kpi/chopping/listForMonth">切片工站月度绩效</a></li>
    </ul>
    <%--查询部分--%>
    <form:form id="searchForm" method="post" action="${ctx}/kpi/chopping/" modelAttribute="chopping" class="breadcrumb form-search">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}">
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}">
        <div class="controls">
            <label>开始时间：</label>
            <form:input path="startTime" maxlength="50" class="input-medium wdate" htmlEscape="false" onclick="WdatePicker({datefmt:'yyyy-mm-dd'})"/>
            &nbsp;&nbsp;
            <label>结束时间：</label>
            <form:input path="endTime" maxlength="50" class="input-medium wdate" htmlEscape="false" onclick="WdatePicker({datefmt:'yyyy-mm-dd'})"/>
            &nbsp;&nbsp;
            <label>人员名称：</label>
            <form:input path="employeeName" maxlength="50" class="input-medium" htmlEscape="false"/>
            <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
                <%--导出excel表功能--%>
            <input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
        </div>
    </form:form>
    <sys:message content="${message}"/>
    <%--列表--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>日期</th>
            <th>人员</th>
            <th>总数（片）</th>
            <th>薪资（元）</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${page.list}" var="chopping">
            <tr>
                <td>${chopping.time}</td>
                <td>${chopping.employeeName}</td>
                <td>${chopping.pieces}</td>
                <td>${chopping.money}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--分页部分--%>
    <div class="pagination">
        ${page}
    </div>
</body>
</html>
