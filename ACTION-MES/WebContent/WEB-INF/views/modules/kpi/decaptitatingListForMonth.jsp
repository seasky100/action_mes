<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<html>
<head>
    <meta name="decorator" content="default">
    <title>去头工站绩效</title>
    <script type="text/javascript">
        function page(n,s) {
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/kpi/decaptitating/listForMonth");
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/kpi/decaptitating/list">去头工站绩效</a></li>
        <li class="active"><a href="${ctx}/kpi/decaptitating/listForMonth">去头工站月度绩效</a></li>
    </ul>
    <%--查询部分--%>
    <form:form id="searchForm" method="post" action="${ctx}/kpi/decaptitating/" modelAttribute="decaptitating" class="breadcrumb form-search">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}">
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}">
        <div class="controls">
            <label>人员名称：</label>
            <form:input path="employeeName" maxlength="50" class="input-medium" htmlEscape="false"/>
            <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
                <%--导出excel表功能--%>
            <input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
        </div>
    </form:form>
    <sys:message content="${message}"/>
    <%--列表--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <th>日期</th>
        <th>人员</th>
        <th>总重（Kg）</th>
        <th>薪资（元）</th>
        </thead>
        <tbody>
        <c:forEach items="${pageForMonth.list}" var="decaptitating">
            <tr>
                <td>${decaptitating.time}</td>
                <td>${decaptitating.employeeName}</td>
                <td>${decaptitating.weight}</td>
                <td>${decaptitating.money}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--分页部分--%>
    <div class="pagination">
        ${pageForMonth}
    </div>
</body>
</html>
