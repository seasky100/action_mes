<%--
  Created by IntelliJ IDEA.
  User: 97674
  Date: 2019/9/26
  Time: 9:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>工作时间统计</title>
</head>
<body>
<%--tab头部--%>
<ul class="nav nav-tabs">
    <li class="active"><a href="${ctx}/kpi/workTime">工作时间统计</a></li>
</ul>
<%--查询部分--%>
<form:form id="searchForm" method="post" action="${ctx}/kpi/workTimelist" modelAttribute="workTime" class="breadcrumb form-search">
    <div class="controls">
        &nbsp;&nbsp
        <label>人员名称：</label>
        <form:input path="employeeName" maxlength="50" class="input-medium wdate" htmlEscape="false"/>
        <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
    </div>
</form:form>

<table id="contentTable" class="table table-striped table-bordered table-condensed">
    <thead>
    <th>员工id</th>
    <th>员工名</th>
    <c:choose>
        <c:when test="${workTimes.get(0).type=='year'}">
            <th>年/
                <a href="${ctx}/kpi/workTime/byMonth">月/</a>
                <a href="${ctx}/kpi/workTime/byWeek">周</a>
            </th>
        </c:when>
        <c:when test="${workTimes.get(0).type=='month'}">
            <th>
                <a href="${ctx}/kpi/workTime/byYear">年/</a>
                月/
                <a href="${ctx}/kpi/workTime/byWeek">周</a>
            </th>
        </c:when>
        <c:when test="${workTimes.get(0).type=='week'}">
            <th>
                <a href="${ctx}/kpi/workTime/byYear">年/</a>
                <a href="${ctx}/kpi/workTime/byMonth">月/</a>
                周
            </th>
        </c:when>
        <c:otherwise>
            <th>ada</th>
        </c:otherwise>
    </c:choose>
    <th>工作时长(小时)</th>
    </thead>
    <tbody>
    <c:forEach items="${workTimes}" var="workTime">
        <tr>
            <td>${workTime.employee_id}</td>
            <td>${workTime.employeeName}</td>
            <td>${workTime.time}</td>
            <td>${workTime.workHours}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
