<%@ page contentType="text/html;charset=GBK" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<html>
<head>
    <meta name="decorator" content="default">
    <title>挑刺工站绩效</title>
    <script type="text/javascript">
        function page(n,s) {
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/kpi/decaptitating/listForMonth");
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
<%--tab头部--%>
<ul class="nav nav-tabs">
    <li class="active"><a href="${ctx}/kpi/removeFishBone/list">挑刺工站绩效</a></li>
    <li><a href="${ctx}/kpi/removeFishBone/listForMonth">挑刺工站月度绩效</a></li>
</ul>
<%--查询部分--%>
<form:form id="searchForm" method="post" action="${ctx}/kpi/removeFishBone/" modelAttribute="removeFishBone" class="breadcrumb form-search">
    <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}">
    <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}">
    <div class="controls">
        <label>人员名称：</label>
        <form:input path="employeeName" maxlength="50" class="input-medium wdate" htmlEscape="false"/>
        <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
            <%--导出excel表功能--%>
        <input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
    </div>
</form:form>
<sys:message content="${message}"/>
<%--列表--%>
<table id="contentTable" class="table table-striped table-bordered table-condensed">
    <thead>
    <th>日期</th>
    <th>人员</th>
    <th>总重（Kg）</th>
    <th>薪资（元）</th>
    </thead>
    <tbody>
    <c:forEach items="${pageForMonth.list}" var="removeFishBone">
        <tr>
            <td>${removeFishBone.time}</td>
            <td>${removeFishBone.employeeName}</td>
            <td>${removeFishBone.weight}</td>
            <td>${removeFishBone.money}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<%--分页部分--%>
<div class="pagination">
    ${pageForMonth}
</div>
</body>
</html>
