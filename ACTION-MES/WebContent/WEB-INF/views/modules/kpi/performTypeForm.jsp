<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>绩效参数管理</title>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/kpi/performType/list/">绩效参数列表</a></li>
        <li class="active"><a href="">绩效参数${not empty line.id?'修改':'添加'}</a> </li>
    </ul>
    <br/>
    <%--表单部分--%>
    <form:form id="inputForm" method="post" action="${ctx}/kpi/performType/save" modelAttribute="performType" class="form-horizontal">
    <form:hidden path="id"/>
    <sys:message content="${message}"/>
        <div class="control-group">
            <label class="control-label">绩效参数名称：</label>
            <div class="controls">
                <form:input path="performTypeName" htmlEscape="false" maxlength="50" class="required"/><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">单价（分）：</label>
            <div class="controls">
                <form:input path="performTypeNum" htmlEscape="false" maxlength="50" class="required"/><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">单位：</label>
            <div class="controls">
                <form:input path="performTypeUnit" htmlEscape="false" maxlength="50" class="required"/><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="form-actions">
            <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
            <input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
        </div>
    </form:form>
</body>
</html>
