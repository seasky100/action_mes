<%--
  Created by IntelliJ IDEA.
  User: 婷婷酱
  Date: 2019/9/12
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="utf-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>维修报告</title>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler:function (form) {
                    loading('正在提交，请稍后...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function (error,element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    }
                    else{
                        error.insertAfter(element);
                    }
                }
            })
        });
    </script>
</head>
<body>
<!--1.tab头部-->
<ul class="nav nav-tabs">
    <li class="active">
        <a href="${ctx}/equip/repair/">${not empty equipRepair.id?'查看':'添加'}维修报告</a>
    </li>
</ul>
<!--2.表单-->
<form:form id="inputForm" method="post" action="${ctx}/equip/repair/save" modelAttribute="equipRepair" class="breadcrumb form-select">
    <form:hidden path="id"/>
    <form:hidden path="mid"/>
    <sys:message content="${message}"/>

    <div class="control-group">
        <lable class="control-label">故障类型:</lable>
        <div class="controls">
            <form:input path="faultType" value="${equipRepair.faultType}"
                        htmlEscape="false" maxlength="50" class="required"
                        readonly="${not empty equipRepair.id?true:false}"/>
            <span class="help-inline"><font color ="red">*</font></span>
        </div>
    </div>

    <div class="control-group">
        <lable class="control-label">故障原因:</lable>
        <div class="controls">
            <form:input path="faultReason" htmlEscape="false" maxlength="50" class="required"
                        readonly="${not empty equipRepair.id?true:false}"/>
            <span class="help-inline"><font color ="red">*</font></span>
        </div>
    </div>


    <div class="control-group">
        <lable class="control-label">故障描述:</lable>
        <div class="controls">
            <form:textarea path="faultDesc" htmlEscape="false" maxlength="200" rows="5" class="required"
                           readonly="${not empty equipRepair.id?true:false}"/>
            <span class="help-inline"><font color="red">*</font></span>
        </div>
    </div>

    <div class="form-actions">

        <input id="btnSubmit" class="btn btn-primary" type="${not empty equipRepair.id?'hidden':'submit'}" value="保存">&nbsp;
        <input id="btnCancel" class="btn btn-primary" value="返回"  onclick="history.go(-1)">
    </div>

</form:form>

</body>
</html>
