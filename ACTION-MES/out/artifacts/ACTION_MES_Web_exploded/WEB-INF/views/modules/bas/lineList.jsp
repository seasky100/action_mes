<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>产线管理</title>
    <script type="text/javascript">
        function page(n,s) {
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li class="active"><a href="${ctx}/bas/line/list">产线列表</a></li>
        <li><a href="${ctx}/bas/line/form">产线添加</a> </li>
    </ul>
    <%--查询部分--%>
    <form:form id="searchForm" method="post" action="${ctx}/bas/line/" modelAttribute="line" class="breadcrumb form-search">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}">
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}">
        <div class="controls"></div>
    </form:form>
    <sys:message content="${message}"/>
    <%--列表部分--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <td>产线名称</td>
            <td>所处车间</td>
            <td>产线编码</td>
            <td>产线负责人</td>
            <td>产线描述</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${page.list}" var="line">
            <tr>
                <td>${line.lineName}</td>
                <td>${line.workShop.shopName}</td>
                <td>${line.lineNumber}</td>
                <td>${line.lineMaster}</td>
                <td>${line.lineDescription}</td>
                <td>
                    <a href="${ctx}/bas/line/form?id=${line.id}">修改</a>
                    <a href="${ctx}/bas/line/delete?id=${line.id}" onclick="return confirm('确认要删除该车间吗？',this.href)">删除</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--分页部分--%>
    <div class="pagination">
        ${page}
    </div>
</body>
</html>
