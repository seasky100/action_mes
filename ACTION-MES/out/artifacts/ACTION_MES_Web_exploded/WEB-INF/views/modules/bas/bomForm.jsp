<%--
  Created by IntelliJ IDEA.
  User: 10720
  Date: 2019/8/29
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>Bom管理</title>
    <script type="text/javascript">
        (document).ready(function(){
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler:function (form) {
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer:"#messageBox",
                errorPlacement:function (error,element) {
                    $("#messageBox").text("输入有误，请先改正");
                    if(element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    }else{
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
<!--1.tab-->
<ul class="nav nav-tabs">
    <li>
        <a href="${ctx}/bas/bom/">Bom列表</a>
    </li>
    <li class="active">
        <a href="">Bom${not empty bom.id?'修改':'添加'}</a>
    </li>
</ul>
<br/>
<!--2.表单-->
<form:form id="searchForm" method="post" action="${ctx}/bas/bom/save" modelAttribute="bom" class="breadcrumb form-select">
    <form:hidden path="id"/>
    <div class="control-group">
        <lable class="control-label">Bom名称：</lable>
        <div class="controls">
            <form:input path="bomName" htmlEscape="false" maxlength="50" class="required"/>
            <span class="help-inline"><font color="red">*</font></span>
        </div>
    </div>
    <div class="control-group">
        <lable class="control-label">Bom版本：</lable>
        <div class="controls">
            <form:input path="bomVersion" htmlEscape="false" maxlength="50" class="required"/>
            <span class="help-inline"><font color="red">*</font></span>
        </div>
    </div>
    <div class="control-group">
        <lable class="control-label">Bom状态：</lable>
        <div class="controls">
            <form:input path="status" htmlEscape="false" maxlength="50" class="required"/>
            <span class="help-inline"><font color="red">*</font></span>
        </div>
    </div>
    <div class="control-group">
        <lable class="control-label">所属产品：</lable>
        <div class="controls">
            <form:select path="product.id" id="pId" class="input-medium required">
                <form:option value="" label=""/>
                <form:options items="${productList}" itemLabel="productName" itemValue="id" htmlEscape="false"/>
            </form:select>
            <span class="help-inline"><font color="red">*</font></span>
        </div>
    </div>
    <div class="form-actions">
        <input id="btnSubmit" class="btn btn-primary" type="submit" value="保存"/>&nbsp;
        <input id="btnCancel" class="btn" type="button" value="返回" onclick="history.go(-1)"/>
    </div>
</form:form>

</body>
</html>
