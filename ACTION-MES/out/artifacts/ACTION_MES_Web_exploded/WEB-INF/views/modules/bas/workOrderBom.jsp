<%--
  Created by IntelliJ IDEA.
  User: z&w
  Date: 2019/9/10
  Time: 上午 10:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%><html>
<head>
    <meta name="decorator" content="default">
    <%--    装饰器模板--%>
    <title>工单管理</title>
    <script type="text/javascript">
        function page(n,s) {
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("searchForm").submit();
        }
    </script>
</head>
<body>
    <%--1.tab--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/bas/workOrder/">工单列表</a></li>
        <li class="active"><a href="">BOM信息</a></li>
    </ul>
    <!--2.查询-->
    <form:form id="searchForm" modelAttribute="workOrder" class="breadcrumb form-select">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
        <lable>工单号：</lable>
        <form:input path="orderCode" maxlength="50" class="input-mdeium" htmlEscape="false" readonly="true"/>
        <lable>产品：</lable>
        <form:input path="product.productName" maxlength="50" class="input-mdeium" htmlEscape="false" readonly="true"/>
        <lable>Bom名称：</lable>
        <form:input path="" value="${bom.bomName}" maxlength="50" class="input-mdeium" htmlEscape="false" readonly="true"/>
    </form:form>
    <!--3.列表-->
    <label style="padding-left: 10px;padding-bottom: 5px">BOM详情</label>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
            <th>材料类型</th>
            <th>材料数量</th>
            <th>单位</th>
        </thead>
        <tbody>
            <c:forEach items="${page.list}" var="bomDetail">
                <tr>
                    <td>${fns:getDictLabel(bomDetail.mType,'meterial_type' ,'' )}</td>
                    <td>${bomDetail.mNum}</td>
                    <td>${fns:getDictLabel(bomDetail.unit,'unit' ,'' )}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <!--4.分页-->
    <div class="pagination">
        ${page}
    </div>
</body>
</html>
