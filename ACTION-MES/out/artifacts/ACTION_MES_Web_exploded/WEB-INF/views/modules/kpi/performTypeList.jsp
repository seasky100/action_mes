<%@ page contentType="text/html;charset=GBK" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>绩效管理</title>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出用户数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/sys/user/export");
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
        });
        function page(n,s) {
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/sys/user/list");
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li class="active"><a href="${ctx}/kpi/performType/list">绩效参数列表</a></li>
        <li><a href="${ctx}/kpi/performType/form">绩效参数添加</a> </li>
    </ul>
    <%--查询部分--%>
    <form:form id="searchForm" method="post" action="${ctx}/kpi/performType/" modelAttribute="performType" class="breadcrumb form-search">
        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}">
        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}">
        <div class="controls">
            <label>绩效参数名称</label>
            <form:input path="performTypeName" maxlength="50" class="input-medium" htmlEscape="false"/>
            <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
        </div>
    </form:form>
    <sys:message content="${message}"/>
    <%--列表部分--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <td>绩效参数名称</td>
            <td>单价（分）</td>
            <td>单位</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${page.list}" var="performType">
            <tr>
                <td>${performType.performTypeName}</td>
                <td>${performType.performTypeNum}</td>
                <td>${performType.performTypeUnit}</td>
                <td>
                    <a href="${ctx}/kpi/performType/form?id=${performType.id}">修改</a>
                    <a href="${ctx}/kpi/performType/delete?id=${performType.id}" onclick="return confirm('确认要删除绩效参数信息吗？',this.href)">删除</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--分页部分--%>
    <div class="pagination">
        ${page}
    </div>
</body>
</html>
