<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>绩效参数与工站关系</title>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/kpi/performStation/list/">绩效参数列表</a></li>
        <li class="active"><a href="">绩效参数${not empty performStation.id?'修改':'添加'}</a> </li>
    </ul>
    <%--表单部分--%>
    <form:form id="inputForm" method="post" action="${ctx}/kpi/performStation/save" modelAttribute="performStation" class="form-horizontal">
    <form:hidden path="id"/>
    <sys:message content="${message}"/>
    <div class="control-group">
        <div class="control-group">
            <label class="control-label">绩效参数：</label>
            <div class="controls">
                <form:select path="performType.id" id="performTypeId" class="input-medium required">
                    <form:option value="" label=""/>
                    <form:options items="${performTypeList}" itemLabel="performTypeName" itemValue="id" htmlEscape="false"/>
                </form:select><span><font color="red" class="help-inline">*</font> </span>
            </div>
        </div>
        <div class="control-group">
            <div class="control-group">
                <label class="control-label">所属工站：</label>
                <div class="controls">
                    <form:select path="workStationInfos.id" id="workStationInfosId" class="input-medium required">
                        <form:option value="id" label="${performStation.workStationInfos.stationName}"/>
                        <form:options items="${stationList}" itemLabel="stationName" itemValue="id" htmlEscape="false"/>
                    </form:select><span><font color="red" class="help-inline">*</font> </span>
                </div>
            </div>
        <div class="form-actions">
            <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
            <input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
        </div>
    </form:form>
</body>
</html>
