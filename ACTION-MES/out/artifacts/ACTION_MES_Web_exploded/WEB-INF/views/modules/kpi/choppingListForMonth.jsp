<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>切片工站绩效</title>
    <script type="text/javascript">
        function page(n,s) {
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/kpi/chopping/listForMonth");
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/kpi/chopping/list">开片工站绩效</a></li>
        <li class="active"><a href="${ctx}/kpi/chopping/listForMonth">开片工站月度绩效</a></li>
    </ul>
    <%--查询部分--%>
    <form:form id="searchForm" method="post" action="${ctx}/kpi/chopping/listForMonth" modelAttribute="chopping" class="breadcrumb form-search">
        <input id="pageNo" name="pageNo" type="hidden" value="${pageForMonth.pageNo}">
        <input id="pageSize" name="pageSize" type="hidden" value="${pageForMonth.pageSize}">
        <div class="controls">
            <label>职员姓名</label>
            <form:input path="workgroupName" maxlength="50" class="input-medium" htmlEscape="false"/>
            <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
        </div>
    </form:form>
    <sys:message content="${message}"/>
    <%--列表部分--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>日期</th>
            <th>人员</th>
            <th>总数（片）</th>
            <th>薪资（元）</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${pageForMonth.list}" var="chopping">
            <tr>
                <td>${chopping.time}</td>
                <td>${chopping.employeeName}</td>
                <td>${chopping.pieces}</td>
                <td>${chopping.money}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--分页部分--%>
    <div class="pagination">
        ${pageForMonth}
    </div>>
</body>
</html>
