<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <meta name="decorator" content="default">
    <title>装袋工站月度绩效</title>
    <script type="text/javascript">
        function page(n,s) {
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/kpi/packages/listForMonth");
            $("#searchForm").submit();
        }
    </script>
</head>
<body>
    <%--tab头部--%>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/kpi/packages/list">装袋工站绩效</a></li>
        <li class="active"><a href="${ctx}/kpi/packages/listForMonth">装袋工站月度绩效</a></li>
    </ul>
    <%--查询部分--%>
    <form:form id="searchForm" method="post" action="${ctx}/kpi/packages/listForMonth" modelAttribute="packages" class="breadcrumb form-search">
        <input id="pageNo" name="pageNo" type="hidden" value="${pageForMonth.pageNo}">
        <input id="pageSize" name="pageSize" type="hidden" value="${pageForMonth.pageSize}">
        <div class="controls">
            <label>职员姓名</label>
            <form:input path="workgroupName" maxlength="50" class="input-medium" htmlEscape="false"/>
            <input id="btnSubmit" type="submit" value="查询" class="btn btn-primary">
        </div>
    </form:form>
    <sys:message content="${message}"/>
    <%--列表部分--%>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>日期</th>
            <th>小组</th>
            <th>袋数（袋）</th>
            <th>薪资（元）</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${pageForMonth.list}" var="packages">
            <tr>
                <td>${packages.time}</td>
                <td>${packages.workgroupName}</td>
                <td>${packages.counter}</td>
                <td>${packages.money}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--分页部分--%>
    <div class="pagination">
        ${pageForMonth}
    </div>
</body>
</html>
