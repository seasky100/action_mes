package cn.action.common.utils;

import cn.action.modules.kpi.entity.ExcelFile;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

public interface ExportExcel{
        void createExcel();
//        String bindData();
        String bindData(String path);
}
