package cn.action.modules.trace.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.modules.bas.entity.Bom;
import cn.action.modules.bas.entity.BomDetail;
import cn.action.modules.bas.entity.Product;
import cn.action.modules.bas.service.BomDetailService;
import cn.action.modules.bas.service.BomService;
import cn.action.modules.bas.service.ProductService;
import cn.action.modules.trace.entity.TraceBomComponent;
import cn.action.modules.trace.service.TraceBomComponentService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "${adminPath}/trace/traceBomComponent")
public class TraceBomComponentController extends BaseController {
    @Autowired
    private TraceBomComponentService traceBomComponentService;
    @ModelAttribute("traceBomComponent")
    public TraceBomComponent get(@RequestParam(required = false) String id)
    {
        System.out.println("1");
        if(StringUtils.isNotBlank(id)){
            return traceBomComponentService.get(id);
        }
        return new TraceBomComponent();
    }

    //根据条件分页查询
    @RequestMapping(value = {"list",""})
    public String list(TraceBomComponent traceBomComponent, HttpServletRequest request, HttpServletResponse response, Model model)
    {
        System.out.println("2");
        Page<TraceBomComponent> page=traceBomComponentService.findPage(new Page<TraceBomComponent>(request,response),traceBomComponent);
        model.addAttribute("page",page);
        System.out.println("3");
        return "modules/trace/traceBomComponentList";
    }
}
