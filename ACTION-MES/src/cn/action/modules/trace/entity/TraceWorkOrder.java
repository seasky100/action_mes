package cn.action.modules.trace.entity;

import cn.action.common.persistence.DataEntity;
import cn.action.modules.bas.entity.Product;
import cn.action.modules.bas.entity.WorkOrder;

public class TraceWorkOrder extends DataEntity<TraceWorkOrder> {
/*

 */
    private  String orderCode;
    private  String orderNo;
    private  String indentor;
    private  Product product;
    private  int quantity;
    private  String unit;
    private  String orderDate;
    private  String salesman;
    private  String status;
    private WorkOrder workOrder;

public TraceWorkOrder(){
        super();
        this.workOrder=new WorkOrder();
        this.product=new Product();
        }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getIndentor() {
        return indentor;
    }

    public void setIndentor(String indentor) {
        this.indentor = indentor;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
}
