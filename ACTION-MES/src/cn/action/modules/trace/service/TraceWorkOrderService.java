package cn.action.modules.trace.service;

import cn.action.common.service.CrudService;
import cn.action.modules.trace.dao.TraceProcessDao;
import cn.action.modules.trace.dao.TraceWorkOrderDao;
import cn.action.modules.trace.entity.TraceProcess;
import cn.action.modules.trace.entity.TraceWorkOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class TraceWorkOrderService extends CrudService<TraceWorkOrderDao, TraceWorkOrder> {
}
