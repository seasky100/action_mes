package cn.action.modules.kpi.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.RemoveFishBone;

import java.util.List;

public interface RemoveFishBoneDao extends CrudDao<RemoveFishBone> {
    public List<Employee> findEmployeeName();
}
