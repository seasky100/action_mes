package cn.action.modules.kpi.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.Packages;

import java.util.List;

public interface PackagesDao extends CrudDao<Packages> {
    public List<Employee> findEmployeeName();
}

