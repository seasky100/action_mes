package cn.action.modules.kpi.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.CutPiece;

import java.util.List;

public interface CutPieceDao extends CrudDao<CutPiece> {
    public List<Employee> findEmployeeName();
}
