package cn.action.modules.kpi.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.Boxing;

import java.util.List;

public interface BoxingDao  extends CrudDao<Boxing> {
    public List<Employee> findEmployeeName();
}
