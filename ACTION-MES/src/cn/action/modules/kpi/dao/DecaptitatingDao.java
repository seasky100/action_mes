package cn.action.modules.kpi.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.Decaptitating;
import cn.action.modules.sys.entity.User;

import java.util.List;

public interface DecaptitatingDao extends CrudDao<Decaptitating> {
    public List<Employee> findEmployeeName();
}
