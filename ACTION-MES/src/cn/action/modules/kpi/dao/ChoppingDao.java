package cn.action.modules.kpi.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.Chopping;

import java.util.List;

public interface ChoppingDao extends CrudDao<Chopping> {
    public List<Employee> findEmployeeName();
}
