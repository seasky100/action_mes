package cn.action.modules.kpi.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.kpi.entity.WorkTime;

import java.util.List;

public interface WorkTimeDao extends CrudDao<WorkTime> {
    public List<WorkTime> findByYear(WorkTime workTime);
    public List<WorkTime> findByMonth(WorkTime workTime);
    public List<WorkTime> findByWeek(WorkTime workTime);
}
