package cn.action.modules.kpi.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.DateUtils;
import cn.action.common.utils.ExportExcel;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.common.web.SverResponse;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.Boxing;
import cn.action.modules.kpi.entity.Decaptitating;
import cn.action.modules.kpi.entity.ExcelFile;
import cn.action.modules.kpi.entity.Packages;
import cn.action.modules.kpi.service.BoxingService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"${adminPath}/kpi/boxing","${exportPath}"})
public class BoxingController extends BaseController implements ExportExcel {
    @Autowired
    private BoxingService boxingService;

    private ExcelFile excelFile;
    private HSSFWorkbook workbook;
    private List<Boxing> data = new ArrayList<>();

    @ModelAttribute("boxing")
    public Boxing get (@RequestParam(required = false)String id){
        if (StringUtils.isNotBlank(id)){
            return boxingService.get(id) ;
        }
        return new Boxing();
    }
    @RequestMapping(value = {"list",""})
    public String list(Boxing boxing, HttpServletRequest request, HttpServletResponse response, Model model){
        Page<Boxing> page = boxingService.findPage(new Page<>(request,response),boxing);
        model.addAttribute("page",page);
        return "modules/kpi/boxingList";
    }

    @RequestMapping(value = "export",method = RequestMethod.POST)
    public String exportFile(Boxing boxing, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes){
        /*查询信息*/
        Page<Boxing> page = boxingService.findPage(new Page<>(request,response),boxing);
        data = page.getList();
        createExcel();

        String path=request.getSession().getServletContext().getRealPath("");//获取项目运行路径
        String message = bindData(path);
        addMessage(redirectAttributes,message);
        return "redirect:"+adminPath+"/kpi/boxing";
    }

    @RequestMapping(value = "listForMonth")
    public String listForMonth(Boxing boxing, HttpServletResponse response, HttpServletRequest request, Model model){
        Page<Boxing> page = boxingService.findListForMonth(new Page<>(request,response),boxing);
        model.addAttribute("pageForMonth",page);
        return "modules/kpi/boxingListForMonth";
    }

    @RequestMapping(value = "findEmployeeName")
    public String findEmployeeName(@RequestHeader("Accept") String accept, HttpServletResponse response){
        List<Employee> employees=boxingService.findEmployeeName();
        return renderString(response, SverResponse.createRespBySuccess(employees));
    }

    @Override
    public void createExcel() {
        /*创建Excel表*/
        workbook = new HSSFWorkbook();
        excelFile = new ExcelFile(workbook,"装箱工站");
        /*创建表头*/
        List<String> title = new ArrayList<>();
        title.add("日期");
        title.add("小组");
        title.add("条形码");
        title.add("箱数（箱）");
        title.add("薪资（元）");
        for (int i = 0;i<title.size();i++){
            HSSFCell cell = excelFile.getTitleRow().createCell(i);
            cell.setCellValue(title.get(i));
            cell.setCellStyle(excelFile.getCellStyle());
        }
    }

    @Override
    public String bindData(String path) {
        //添加数据内容
        for (int i = 0;i<data.size();i++){
            Boxing boxing = data.get(i);
            excelFile.setDataRow(excelFile.getSheet().createRow(i+1));

            //创建单元格
            HSSFCell cell = excelFile.getDataRow().createCell(0);
            cell.setCellValue(boxing.getTime());
            cell.setCellStyle(excelFile.getCellStyle());

            cell = excelFile.getDataRow().createCell(1);
            cell.setCellValue(boxing.getWorkgroupName());
            cell.setCellStyle(excelFile.getCellStyle());


            cell = excelFile.getDataRow().createCell(2);
            cell.setCellValue(boxing.getBarcode());
            cell.setCellStyle(excelFile.getCellStyle());


            cell = excelFile.getDataRow().createCell(3);
            cell.setCellValue(boxing.getCounter());
            cell.setCellStyle(excelFile.getCellStyle());

            cell = excelFile.getDataRow().createCell(4);
            cell.setCellValue(boxing.getMoney());
            cell.setCellStyle(excelFile.getCellStyle());
        }

        //保存Excel文件
        try{
            String fileName = "装箱工站绩效"+ DateUtils.getDate("yyyyMMddHHmmss")+".xls";
            OutputStream outputStream = new FileOutputStream(path+fileName);
            workbook.write(outputStream);
            outputStream.close();
            return "导出装箱工站绩效成功！";
        }catch (Exception e){
            return "导出装箱工站绩效失败！失败信息："+e.getMessage();
        }
    }
}
