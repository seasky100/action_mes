package cn.action.modules.kpi.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.modules.kpi.entity.WorkTime;
import cn.action.modules.kpi.service.WorkTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = {"${adminPath}/kpi/workTime"})
public class WorkTimeController extends BaseController{
    @Autowired
    private WorkTimeService workTimeService;

    @ModelAttribute("workTime")
    public WorkTime get(@RequestParam(required = false) String id){
        if (StringUtils.isNotBlank(id)){
            return workTimeService.get(id);
        }
        return new WorkTime();
    }

    @RequestMapping(value ={"list",""})
    public String list(WorkTime workTime,HttpServletResponse response, HttpServletRequest request, Model model){
        List<WorkTime> workTimes =workTimeService.findByYear(workTime);
        model.addAttribute("workTimes",workTimes);

        return "modules/kpi/workTimeList";
    }

    @RequestMapping(value ={"byYear"})
    public String byYear(WorkTime workTime,HttpServletResponse response, HttpServletRequest request, Model model){
        List<WorkTime> workTimes =workTimeService.findByYear(workTime);
        model.addAttribute("workTimes",workTimes);

        return "modules/kpi/workTimeList";
    }
    @RequestMapping(value ={"byMonth"})
    public String byMonth(WorkTime workTime,HttpServletResponse response, HttpServletRequest request, Model model){
        List<WorkTime> workTimes =workTimeService.findByMonth(workTime);
        model.addAttribute("workTimes",workTimes);

        return "modules/kpi/workTimeList";
    }
    @RequestMapping(value ={"byWeek"})
    public String byWeek(WorkTime workTime,HttpServletResponse response, HttpServletRequest request, Model model){
        List<WorkTime> workTimes =workTimeService.findByWeek(workTime);
        model.addAttribute("workTimes",workTimes);

        return "modules/kpi/workTimeList";
    }
}
