package cn.action.modules.kpi.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.DateUtils;
import cn.action.common.utils.ExportExcel;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.common.web.Response;
import cn.action.common.web.SverResponse;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.Boxing;
import cn.action.modules.kpi.entity.Chopping;
import cn.action.modules.kpi.entity.Decaptitating;
import cn.action.modules.kpi.entity.ExcelFile;
import cn.action.modules.kpi.service.ChoppingService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"${adminPath}/kpi/chopping","${exportPath}"})
public class ChoppingController extends BaseController implements ExportExcel {
    @Autowired
    private ChoppingService choppingService;

    private ExcelFile excelFile;
    private HSSFWorkbook workbook;
    private List<Chopping> data = new ArrayList<>();

    @ModelAttribute("chopping")
    public Chopping get(@RequestParam(required = false) String id){
        if (StringUtils.isNotBlank(id)){
            return choppingService.get(id);
        }
        return new Chopping();
    }
    //按条件分页查询
    @RequestMapping(value = {"list",""})
    public String list(Chopping chopping, HttpServletResponse response, HttpServletRequest request, Model model){
        Page<Chopping> page = choppingService.findPage(new Page<>(request,response),chopping);
        model.addAttribute("page",page);

        String accept=request.getHeader("accept");//从request中取accept参数（也可用@RequestHeader注解取参数）
        String json=renderString(response, SverResponse.createRespBySuccess(page));
        String url="modules/kpi/choppingList";
        return Response.CreateResponse(accept,json,url);//根据accept参数返回json或url字符串
    }

    @RequestMapping(value = "export",method = RequestMethod.POST)
    public String exportFile(Chopping chopping, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes){
        /*查询信息*/
        Page<Chopping> page = choppingService.findPage(new Page<>(request,response),chopping);
        data = page.getList();
        createExcel();

        String path=request.getSession().getServletContext().getRealPath("");//获取项目运行路径
        String message = bindData(path);
        addMessage(redirectAttributes,message);
        return "redirect:"+adminPath+"/kpi/chopping";
    }

    @RequestMapping(value = "listForMonth")
    public String listForMonth(Chopping chopping, HttpServletResponse response, HttpServletRequest request, Model model){
        Page<Chopping> page = choppingService.findListForMonth(new Page<>(request,response),chopping);
        model.addAttribute("pageForMonth",page);
        return "modules/kpi/choppingListForMonth";
    }

    @RequestMapping(value = "findEmployeeName")
    public String findEmployeeName(@RequestHeader("Accept") String accept, HttpServletResponse response){
        List<Employee> employees=choppingService.findEmployeeName();
        return renderString(response,SverResponse.createRespBySuccess(employees));
    }

    @Override
    public void createExcel() {
        /*创建Excel表*/
        workbook = new HSSFWorkbook();
        excelFile = new ExcelFile(workbook,"切片工站");
        /*创建表头*/
        List<String> title = new ArrayList<>();
        title.add("日期");
        title.add("人员");
        title.add("总数（片）");
        title.add("薪资（元）");
        for (int i = 0;i<title.size();i++){
            HSSFCell cell = excelFile.getTitleRow().createCell(i);
            cell.setCellValue(title.get(i));
            cell.setCellStyle(excelFile.getCellStyle());
        }
    }

    @Override
    public String bindData(String path) {
        /*添加数据内容*/
        for (int i = 0;i<data.size();i++){
            Chopping chopping = data.get(i);
            excelFile.setDataRow(excelFile.getSheet().createRow(i+1));

            //创建单元格
            HSSFCell cell = excelFile.getDataRow().createCell(0);
            cell.setCellValue(chopping.getTime());
            cell.setCellStyle(excelFile.getCellStyle());

            cell = excelFile.getDataRow().createCell(1);
            cell.setCellValue(chopping.getEmployeeName());
            cell.setCellStyle(excelFile.getCellStyle());


            cell = excelFile.getDataRow().createCell(2);
            cell.setCellValue(chopping.getPieces());
            cell.setCellStyle(excelFile.getCellStyle());


            cell = excelFile.getDataRow().createCell(3);
            cell.setCellValue(chopping.getMoney());
            cell.setCellStyle(excelFile.getCellStyle());
        }

        //保存Excel文件
        try{
            String fileName = "切片工站绩效"+ DateUtils.getDate("yyyyMMddHHmmss")+".xls";
            OutputStream outputStream = new FileOutputStream(path+fileName);
            workbook.write(outputStream);
            outputStream.close();
            return "导出切片工站绩效成功！";
        }catch (Exception e){
            return "导出切片工站绩效失败！失败信息："+e.getMessage();
        }
    }
}
