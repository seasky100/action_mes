package cn.action.modules.kpi.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.DateUtils;
import cn.action.common.utils.ExportExcel;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.common.web.SverResponse;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.Boxing;
import cn.action.modules.kpi.entity.ExcelFile;
import cn.action.modules.kpi.entity.Packages;
import cn.action.modules.kpi.service.PackagesService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"${adminPath}/kpi/packages","${exportPath}"})
public class PackagesController extends BaseController implements ExportExcel {
    @Autowired
    private PackagesService packageService;

    private ExcelFile excelFile;
    private HSSFWorkbook workbook;
    private List<Packages> data = new ArrayList<>();

    @ModelAttribute("packages")
    public Packages get(@RequestParam(required = false)String id){
        if (StringUtils.isNotBlank(id)){
            return packageService.get(id);
        }
        return new Packages();
    }
    @RequestMapping(value = {"list",""})
    public String list(Packages packages, HttpServletResponse response, HttpServletRequest request, Model model) {
        Page<Packages> page = packageService.findPage(new Page<>(request,response),packages);
        model.addAttribute("page",page);
        return "modules/kpi/packagesList";
    }
    @RequestMapping(value = "export",method = RequestMethod.POST)
        public String exportFile(Packages packages, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes){
            /*查询信息*/
            Page<Packages> page = packageService.findPage(new Page<>(request,response),packages);
            data = packageService.findList(packages);
//            filePosition = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/kpi_Export/";
            createExcel();
        String path=request.getSession().getServletContext().getRealPath("");//获取项目运行路径
        String message = bindData(path);
        addMessage(redirectAttributes,message);
        return "redirect:"+adminPath+"/kpi/packages";
    }
    @RequestMapping(value = "listForMonth")
    public String listForMonth(Packages packages, HttpServletResponse response, HttpServletRequest request, Model model){
        Page<Packages> page = packageService.findListForMonth(new Page<>(request,response),packages);
        model.addAttribute("pageForMonth",page);
        return "modules/kpi/packagesListForMonth";
    }

    @RequestMapping(value = "findEmployeeName")
    public String findEmployeeName(@RequestHeader("Accept") String accept, HttpServletResponse response){
        List<Employee> employees=packageService.findEmployeeName();
        return renderString(response, SverResponse.createRespBySuccess(employees));
    }
    @Override
    public void createExcel() {
        /*创建Excel表*/
        workbook = new HSSFWorkbook();
        excelFile = new ExcelFile(workbook,"装袋工站");
        /*创建表头*/
        List<String> title = new ArrayList<>();
        title.add("日期");
        title.add("小组");
        title.add("袋数（袋）");
        title.add("薪资（元）");
        for (int i = 0;i<title.size();i++){
            HSSFCell cell = excelFile.getTitleRow().createCell(i);
            cell.setCellValue(title.get(i));
            cell.setCellStyle(excelFile.getCellStyle());
        }

    }

    @Override
    public String bindData(String path) {
            //添加数据内容
            for (int i = 0;i<data.size();i++){
                Packages packages= data.get(i);
                excelFile.setDataRow(excelFile.getSheet().createRow(i+1));
                //创建单元格
                HSSFRow dataRow = excelFile.getSheet().createRow(i+1);
                excelFile.setDataRow(dataRow);
                HSSFCell cell = excelFile.getDataRow().createCell(0);
                cell.setCellValue(packages.getTime());
                cell.setCellStyle(excelFile.getCellStyle());

                cell = excelFile.getDataRow().createCell(1);
                cell.setCellValue(packages.getWorkgroupName());
                cell.setCellStyle(excelFile.getCellStyle());


                cell = excelFile.getDataRow().createCell(2);
                cell.setCellValue(packages.getCounter());
                cell.setCellStyle(excelFile.getCellStyle());


                cell = excelFile.getDataRow().createCell(3);
                cell.setCellValue(packages.getMoney());
                cell.setCellStyle(excelFile.getCellStyle());
            }

            //保存Excel文件
            try{
                String fileName = "装袋工站绩效"+ DateUtils.getDate("yyyyMMddHHmmss")+".xls";
                OutputStream outputStream = new FileOutputStream(path+fileName);
                workbook.write(outputStream);
                outputStream.close();
                return "导出装袋工站绩效成功！";
            }catch (Exception e){
                return "导出装袋工站绩效失败！失败信息："+e.getMessage();
            }
    }
}
