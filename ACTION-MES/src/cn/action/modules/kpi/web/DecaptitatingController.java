package cn.action.modules.kpi.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.DateUtils;
import cn.action.common.utils.ExportExcel;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.common.web.Response;
import cn.action.common.web.SverResponse;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.entity.CutPiece;
import cn.action.modules.kpi.entity.Decaptitating;
import cn.action.modules.kpi.entity.ExcelFile;
import cn.action.modules.kpi.service.DecaptitatingService;
import cn.action.modules.sys.entity.User;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"${adminPath}/kpi/decaptitating","${exportPath}"})
public class DecaptitatingController extends BaseController implements ExportExcel{
    @Autowired
    private DecaptitatingService decaptitatingService;


     private ExcelFile excelFile;
     private HSSFWorkbook workbook;
     private List<Decaptitating> data = new ArrayList<>();


    @ModelAttribute("decaptitating")
    public Decaptitating get(@RequestParam(required = false) String id){
        if (StringUtils.isNotBlank(id)){
            return decaptitatingService.get(id);
        }
        return new Decaptitating();
    }
    //按条件分页查询
    @RequestMapping(value = {"list",""})
    public String list(Decaptitating decaptitating, HttpServletResponse response, HttpServletRequest request, Model model){
        Page<Decaptitating> page = decaptitatingService.findPage(new Page<>(request,response),decaptitating);
        model.addAttribute("page",page);

        String accept=request.getHeader("accept");//从request中取accept参数（也可用@RequestHeader注解取参数）
        String json=renderString(response, SverResponse.createRespBySuccess(page));
        String url="modules/kpi/decaptitatingList";
        return Response.CreateResponse(accept,json,url);//根据accept参数返回json或url字符串
    }

    @RequestMapping(value = "export",method = RequestMethod.POST)
    public String exportFile(Decaptitating decaptitating, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes){
        /*查询信息*/
        Page<Decaptitating> page = decaptitatingService.findPage(new Page<>(request,response),decaptitating);
        data = page.getList();
        createExcel();

        String path=request.getSession().getServletContext().getRealPath("");//获取项目运行路径
        String message = bindData(path);
        addMessage(redirectAttributes,message);
        return "redirect:"+adminPath+"/kpi/decaptitating";
    }

    @RequestMapping(value = "listForMonth")
    public String listForMonth(Decaptitating decaptitating, HttpServletResponse response, HttpServletRequest request, Model model){
        Page<Decaptitating> page = decaptitatingService.findListForMonth(new Page<>(request,response),decaptitating);
        model.addAttribute("pageForMonth",page);
        return "modules/kpi/decaptitatingListForMonth";
    }

    @RequestMapping(value = "findEmployeeName")
    public String findEmployeeName(@RequestHeader("Accept") String accept, HttpServletResponse response){
        List<Employee> employees=decaptitatingService.findEmployeeName();
        return renderString(response,SverResponse.createRespBySuccess(employees));
    }

    @Override
    public void createExcel() {
        /*创建Excel表*/
        workbook = new HSSFWorkbook();
        excelFile = new ExcelFile(workbook,"开头工站");
        /*创建表头*/
        List<String> title = new ArrayList<>();
        title.add("日期");
        title.add("人员");
        title.add("总重（Kg）");
        title.add("薪资（元）");
        for (int i = 0;i<title.size();i++){
            HSSFCell cell = excelFile.getTitleRow().createCell(i);
            cell.setCellValue(title.get(i));
            cell.setCellStyle(excelFile.getCellStyle());
        }
    }

    @Override
    public String bindData(String path) {
        //添加数据内容
        for (int i = 0;i<data.size();i++){
            Decaptitating decaptitating = data.get(i);
            excelFile.setDataRow(excelFile.getSheet().createRow(i+1));

            //创建单元格
            HSSFCell cell = excelFile.getDataRow().createCell(0);
            cell.setCellValue(decaptitating.getTime());
            cell.setCellStyle(excelFile.getCellStyle());

            cell = excelFile.getDataRow().createCell(1);
            cell.setCellValue(decaptitating.getEmployeeName());
            cell.setCellStyle(excelFile.getCellStyle());


            cell = excelFile.getDataRow().createCell(2);
            cell.setCellValue(decaptitating.getWeight());
            cell.setCellStyle(excelFile.getCellStyle());


            cell = excelFile.getDataRow().createCell(3);
            cell.setCellValue(decaptitating.getMoney());
            cell.setCellStyle(excelFile.getCellStyle());
        }

        //保存Excel文件
        try{
            String fileName = "去头工站绩效"+ DateUtils.getDate("yyyyMMddHHmmss")+".xls";
            OutputStream outputStream = new FileOutputStream(path+fileName);
            workbook.write(outputStream);
            outputStream.close();
            return "导出去头工站绩效成功！";
        }catch (Exception e){
            return "导出去头工站绩效失败！失败信息："+e.getMessage();
        }
    }
}
