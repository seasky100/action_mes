package cn.action.modules.kpi.entity;

import cn.action.common.persistence.DataEntity;

public class WorkTime extends DataEntity<WorkTime> {
    /**
     * 工作时间实体类
     */
    private static final long serialVersionUID = 1L;
    private String employee_id;//员工id
    private String employeeName;//员工名
    private int workHours;//工作时间
    private String type;//时间间隔类型
    private int time;//第几周，几年，几月

    public int getWorkHours() {
        return workHours;
    }

    public void setWorkHours(int workHours) {
        this.workHours = workHours;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }


    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
}
