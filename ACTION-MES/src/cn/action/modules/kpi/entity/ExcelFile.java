package cn.action.modules.kpi.entity;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


public class ExcelFile {
    private HSSFWorkbook workbook; //excel文件
    private String sheetName;//sheet表名
    private HSSFSheet sheet;//sheet表
    private HSSFCellStyle cellStyle;//单元格格式
    private HSSFRow titleRow;//表标题行
    private HSSFRow dataRow;//表内容行
    public ExcelFile(HSSFWorkbook workbook,String sheetName){
        this.workbook = workbook;
        this.sheetName = sheetName;
        this.cellStyle = workbook.createCellStyle();
        this.cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        this.sheet = workbook.createSheet(sheetName);
        this.titleRow = sheet.createRow(0);
        this.dataRow = sheet.createRow(1);
    }

    public HSSFWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(HSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public HSSFSheet getSheet() {
        return sheet;
    }

    public void setSheet(HSSFSheet sheet) {
        this.sheet = sheet;
    }

    public HSSFCellStyle getCellStyle() {
        return cellStyle;
    }

    public void setCellStyle(HSSFCellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }

    public HSSFRow getTitleRow() {
        return titleRow;
    }

    public void setTitleRow(HSSFRow titleRow) {
        this.titleRow = titleRow;
    }

    public HSSFRow getDataRow() {
        return dataRow;
    }

    public void setDataRow(HSSFRow dataRow) {
        this.dataRow = dataRow;
    }
}
