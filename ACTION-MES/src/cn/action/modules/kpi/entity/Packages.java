package cn.action.modules.kpi.entity;

import cn.action.common.persistence.DataEntity;

public class Packages extends DataEntity<Packages> {
    /*
     *装袋工站绩效实体类
     * */

    private static final long serialVersionUID = 1L;

    private String time;//日期
    private String workgroupName;//小组名称
    private String counter;//总袋数
    private String money;//薪资
    private String startTime;//开始时间
    private String endTime;//结束时间

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWorkgroupName() {
        return workgroupName;
    }

    public void setWorkgroupName(String workgroupName) {
        this.workgroupName = workgroupName;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
