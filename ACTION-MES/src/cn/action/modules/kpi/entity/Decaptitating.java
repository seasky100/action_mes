package cn.action.modules.kpi.entity;


import cn.action.common.persistence.DataEntity;

public class Decaptitating extends DataEntity<Decaptitating> {
    /*
     *去头工站绩效实体类
     * */

    private static final long serialVersionUID = 1L;
    private String time;//日期
    private String employeeName;//人员名称
    private String weight;//重量
    private String money;//薪资
    private String startTime;//开始时间
    private String endTime;//结束时间
    private String stationId;//工站id
    private String workCellId;//工位id

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getWorkCellId() {
        return workCellId;
    }

    public void setWorkCellId(String workCellId) {
        this.workCellId = workCellId;
    }
}
