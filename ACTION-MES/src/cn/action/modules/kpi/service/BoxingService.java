package cn.action.modules.kpi.service;

import cn.action.common.service.CrudService;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.dao.BoxingDao;
import cn.action.modules.kpi.entity.Boxing;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class BoxingService extends CrudService<BoxingDao, Boxing> {
    public List<Employee> findEmployeeName(){
        return this.dao.findEmployeeName();
    }
}
