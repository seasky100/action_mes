package cn.action.modules.kpi.service;

import cn.action.common.persistence.CrudDao;
import cn.action.common.service.CrudService;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.dao.DecaptitatingDao;
import cn.action.modules.kpi.entity.Decaptitating;
import cn.action.modules.sys.entity.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class DecaptitatingService extends CrudService<DecaptitatingDao, Decaptitating> {
    public List<Employee> findEmployeeName(){
        return this.dao.findEmployeeName();
    }
}
