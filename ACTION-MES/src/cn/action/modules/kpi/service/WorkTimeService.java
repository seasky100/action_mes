package cn.action.modules.kpi.service;

import cn.action.common.service.CrudService;
import cn.action.modules.kpi.dao.WorkTimeDao;
import cn.action.modules.kpi.entity.WorkTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class WorkTimeService extends CrudService<WorkTimeDao, WorkTime> {
    public List<WorkTime> findByYear(WorkTime workTime){
        return this.dao.findByYear(workTime);
    }
    public List<WorkTime> findByMonth(WorkTime workTime){
        return this.dao.findByMonth(workTime);
    }
    public List<WorkTime> findByWeek(WorkTime workTime){
        return this.dao.findByWeek(workTime);
    }

}
