package cn.action.modules.kpi.service;

import cn.action.common.service.CrudService;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.kpi.dao.PackagesDao;
import cn.action.modules.kpi.entity.Packages;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class PackagesService extends CrudService<PackagesDao, Packages> {
    public List<Employee> findEmployeeName(){
        return this.dao.findEmployeeName();
    }

}
