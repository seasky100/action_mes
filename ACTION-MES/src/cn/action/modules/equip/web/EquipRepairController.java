package cn.action.modules.equip.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.common.web.Response;
import cn.action.common.web.SverResponse;
import cn.action.modules.equip.entity.EquipRepair;
import cn.action.modules.equip.entity.Equipment;
import cn.action.modules.equip.service.EquipRepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "${adminPath}/equip/repair")
public class EquipRepairController extends BaseController {

    @Autowired
    private EquipRepairService equipRepairService;

    @ModelAttribute("equipRepair")
    public EquipRepair get(@RequestParam(required = false) String id, String mid){
        if(StringUtils.isNotBlank(id)){
            return equipRepairService.get(id);
        }
        EquipRepair repair=new EquipRepair();
        repair.setMid(mid);
        return repair;
    }



    //保存维修报告
    @RequestMapping(value="save")
    public String save(EquipRepair equipRepair, Model model, @RequestHeader("Accept") String accept, RedirectAttributes redirectAttributes,HttpServletResponse response){
        //修改上报记录的状态为“完工”，并且保存维修报告
        equipRepairService.saveRepair(equipRepair);
        this.addMessage(redirectAttributes,"维修报告添加成功！");
        String url="redirect:"+adminPath+"/equip/report/repairList";
        String json=renderString(response, SverResponse.createRespBySuccessMessage("维修报告添加成功！"));
        return Response.CreateResponse(accept,json,url);
    }

    @RequestMapping(value = "form")
    public String form(EquipRepair equipRepair,Model model){
        model.addAttribute("equipRepair",equipRepair);
        return "modules/equip/maintenanceReportForm";
    }
}
