package cn.action.modules.equip.web;

//拦截请求，调用service的方法，返回相应的页面路径（数据）。


import cn.action.common.persistence.CrudDao;
import cn.action.common.persistence.Page;
import cn.action.common.utils.DateUtils;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.common.web.Response;
import cn.action.common.web.SverResponse;
import cn.action.modules.bas.entity.Line;
import cn.action.modules.bas.service.LineService;
import cn.action.modules.equip.entity.EquipFaultReport;
import cn.action.modules.equip.entity.Equipment;
import cn.action.modules.equip.entity.MaintenancePlan;
import cn.action.modules.equip.service.EquipFaultReportService;

import org.apache.batik.css.engine.value.css2.SrcManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value="${adminPath}/equip/report")
public class EquipFaultReportController extends BaseController {

    @Autowired
    private EquipFaultReportService equipFaultReportService;

    @Autowired
    private LineService lineService;

    @ModelAttribute("equipFaultReport")
    public EquipFaultReport get(@RequestParam(required = false) String id){
        if(StringUtils.isNotBlank(id)){
            return equipFaultReportService.get(id);
        }
        return new EquipFaultReport();
    }

    //设备故障上报保存
    @RequestMapping(value="save")
    public String save(EquipFaultReport equipFaultReport, Model model, RedirectAttributes redirectAttributes){
        boolean flag=equipFaultReportService.saveFaultReport(equipFaultReport);
        String messages="设备编号有误，无此设备!";
        if(flag){
            messages="故障上报成功！";
        }
        this.addMessage(redirectAttributes,messages);
        return "redirect:"+adminPath+"/equip/report/form";
    }

    @RequestMapping(value="form")
    public String form(EquipFaultReport equipFaultReport,Model model){
        //获得所有产线信息
        List<Line> lines=lineService.findAllList(new Line());
        model.addAttribute("lineList",lines);
        return "modules/equip/equipFaultReportForm";
    }

    //按照条件分页查询
    @RequestMapping(value={"list",""})
    public String list(EquipFaultReport equipFaultReport, HttpServletRequest request, HttpServletResponse response,Model model){
        //获得所有产线信息
        List<Line> lines=lineService.findAllList(new Line());
        model.addAttribute("lineList",lines);
        //获得分页对象
        Page<EquipFaultReport> page = equipFaultReportService.findPage(new Page<EquipFaultReport>(request,response),equipFaultReport);
        model.addAttribute("page",page);

        String accept=request.getHeader("accept");//从request中取accept参数（也可用@RequestHeader注解取参数）
        String json=renderString(response, SverResponse.createRespBySuccess(page));
        String url="modules/equip/equipFaultReportList";
        return Response.CreateResponse(accept,json,url);

    }

    //派工
    @RequestMapping(value="assign")
    public String assign(EquipFaultReport equipFaultReport, Model model, RedirectAttributes redirectAttributes,
                         @RequestHeader("Accept") String accept, HttpServletResponse response){
        //判断是否已经派工
        boolean flag=equipFaultReportService.isAssign(equipFaultReport);
        String url="";
        if(flag){
            //获得所有产线信息
            List<Line> lines=lineService.findAllList(new Line());
            model.addAttribute("lineList",lines);
            url="modules/equip/equipMaintenanceWorkerForm";
            String json=renderString(response,SverResponse.createRespBySuccessMessage("未派工！"));
            return Response.CreateResponse(accept,json,url);

        }
        this.addMessage(redirectAttributes,"该报修已经得到处理，不能重复派工！");
        url="redirect:"+adminPath+"/equip/report";
        String json=renderString(response,SverResponse.createByErrorMessage("该报修已经得到处理，不能重复派工！"));
        return Response.CreateResponse(accept,json,url);
    }

    //派工保存
    @RequestMapping(value="maintenance")
    public String maintenance(EquipFaultReport equipFaultReport, Model model,RedirectAttributes redirectAttributes,
                              @RequestHeader("Accept") String accept, HttpServletResponse response){
        equipFaultReport=equipFaultReportService.get(equipFaultReport);
        if (!"0001".equals(equipFaultReport.getStatus())){
            String json=renderString(response,SverResponse.createByErrorMessage("设备已派工！"));
            String url="redirect:"+adminPath+"/equip/report/list";
            return Response.CreateResponse(accept,json,url);
        }

        equipFaultReport.setStatus("0002");
        equipFaultReport.setAssignTime(DateUtils.formatDateTime(new Date()));
        equipFaultReportService.save(equipFaultReport);
        this.addMessage(redirectAttributes,"派工成功！");

        String json=renderString(response,SverResponse.createRespBySuccessMessage("派工成功！"));
        String url="redirect:"+adminPath+"/equip/report/list";
        return Response.CreateResponse(accept,json,url);
    }

    //按照条件分页查询,跳转到设备维修记录页面
    @RequestMapping(value={"repairList"})
    public String repairList(EquipFaultReport equipFaultReport, HttpServletRequest request, HttpServletResponse response,Model model){
        //获得所有产线信息
        List<Line> lines=lineService.findAllList(new Line());
        model.addAttribute("lineList",lines);
        //获得分页对象
        Page<EquipFaultReport> page = equipFaultReportService.findPage(new Page<EquipFaultReport>(request,response),equipFaultReport);
        model.addAttribute("page",page);

        String accept=request.getHeader("accept");//从request中取accept参数（也可用@RequestHeader注解取参数）
        String json=renderString(response, SverResponse.createRespBySuccess(page));//转换成json字符串
        String url="modules/equip/equipRepairList";//要返回的url
        return Response.CreateResponse(accept,json,url);//根据accept参数返回json或url字符串
    }
    //开工
    @RequestMapping(value = "start")
    public String startRepair(EquipFaultReport equipFaultReport,Model model,RedirectAttributes redirectAttributes,
                              @RequestHeader("Accept") String accept,HttpServletResponse response){
        String message=equipFaultReportService.saveStartRepair(equipFaultReport);
        this.addMessage(redirectAttributes,message);

        String json=renderString(response,SverResponse.createRespBySuccess(message));
        String url="redirect:"+adminPath+"/equip/report/repairList";
        return Response.CreateResponse(accept,json,url);
    }
}
