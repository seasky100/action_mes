package cn.action.modules.equip.service;

import cn.action.common.service.CrudService;
import cn.action.modules.equip.dao.EquipRepairDao;
import cn.action.modules.equip.entity.EquipFaultReport;
import cn.action.modules.equip.entity.EquipRepair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
@Transactional(readOnly = true)
public class EquipRepairService extends CrudService<EquipRepairDao,EquipRepair> {

    @Autowired
    private EquipFaultReportService equipFaultReportService;

    public void saveRepair(EquipRepair equipRepair){
        //修改上报记录状态为完工
        EquipFaultReport equipFaultReport=equipFaultReportService.get(equipRepair.getMid());
        equipFaultReport.setStatus("0004");
        equipFaultReportService.save(equipFaultReport);
        //保存维修报告
        this.save(equipRepair);
    }
}
