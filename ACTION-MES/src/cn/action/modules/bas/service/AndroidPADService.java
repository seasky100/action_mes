package cn.action.modules.bas.service;

import cn.action.common.service.CrudService;
import cn.action.modules.bas.dao.AndroidPADDao;
import cn.action.modules.bas.entity.AndroidPAD;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class AndroidPADService extends CrudService<AndroidPADDao, AndroidPAD> {

}
