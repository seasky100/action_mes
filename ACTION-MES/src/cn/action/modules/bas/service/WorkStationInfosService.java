package cn.action.modules.bas.service;

import cn.action.common.service.CrudService;
import cn.action.modules.bas.dao.WorkStationInfosDao;
import cn.action.modules.bas.entity.WorkStationInfos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class WorkStationInfosService extends CrudService<WorkStationInfosDao, WorkStationInfos> {
    /*
    *
    * 查询没有关联绩效参数的工站的集合
    * */
    public List<WorkStationInfos> findNoPerform(WorkStationInfos workStationInfos){
        return this.dao.findNoPerform(workStationInfos);
    }
}
