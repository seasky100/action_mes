package cn.action.modules.bas.service;

import cn.action.common.service.CrudService;
import cn.action.modules.bas.dao.CellEmployeeDao;
import cn.action.modules.bas.entity.CellEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class CellEmployeeService extends CrudService<CellEmployeeDao, CellEmployee> {

    @Autowired(required = false)
    private CellEmployeeDao cellEmployeeDao;

    /**
     * 保存
     * @param cellEmployee
     * @return
     */
    public CellEmployee saveRelation(CellEmployee cellEmployee){
        //判断是添加还是修改，添加需判断员工是否已绑定工位，其他正常
        if ("".equals(cellEmployee.getId())){
            //根据员工id判断是否绑定了工位，如果绑定，返回关系对象
            CellEmployee relation=cellEmployeeDao.findByEmployeeId(cellEmployee);
           if (relation!=null){
               //该员工已经绑定工位
               return relation;
           }
        }
        this.save(cellEmployee);
        return null;
    }
}
