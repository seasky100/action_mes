package cn.action.modules.bas.service;

import cn.action.common.service.CrudService;
import cn.action.modules.bas.dao.CardReaderDao;
import cn.action.modules.bas.entity.CardReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class CardReaderService extends CrudService<CardReaderDao, CardReader> {
}
