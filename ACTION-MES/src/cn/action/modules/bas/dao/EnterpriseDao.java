package cn.action.modules.bas.dao;

import cn.action.common.persistence.CrudDao;
import cn.action.modules.bas.entity.Enterprise;

/**
 *企业Dao接口
 */
public interface EnterpriseDao extends CrudDao<Enterprise> {
}
