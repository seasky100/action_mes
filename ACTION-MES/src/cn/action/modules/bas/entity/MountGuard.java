package cn.action.modules.bas.entity;

import cn.action.common.persistence.DataEntity;
import cn.action.modules.sys.entity.Office;

/**
 *
 */
public class MountGuard extends DataEntity<MountGuard> {
    /**
     * 员工工作记录
     */
private static final long serialVersionUID = 1L;
//创建MountGuard对象的时候，让这几个外键对象也都分配空间实例化
    public MountGuard(){
        super();
        this.employee=new Employee();
        this.office=new Office();
        this.line=new Line();
        this.workStationInfos=new WorkStationInfos();
        this.workCell=new WorkCell();
    }
    private Employee employee;
    private Office office;
    private Line line;
    private WorkStationInfos workStationInfos;
    private WorkCell workCell;
    private String clockIn;//上班时间
    private String clockOff;//下班时间
    private String workStatus;//工作状态

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public WorkStationInfos getWorkStationInfos() {
        return workStationInfos;
    }

    public void setWorkStationInfos(WorkStationInfos workStationInfos) {
        this.workStationInfos = workStationInfos;
    }

    public WorkCell getWorkCell() {
        return workCell;
    }

    public void setWorkCell(WorkCell workCell) {
        this.workCell = workCell;
    }

    public String getClockIn() {
        return clockIn;
    }

    public void setClockIn(String clockIn) {
        this.clockIn = clockIn;
    }

    public String getClockOff() {
        return clockOff;
    }

    public void setClockOff(String clockOff) {
        this.clockOff = clockOff;
    }

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }
}
