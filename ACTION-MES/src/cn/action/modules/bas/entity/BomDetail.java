package cn.action.modules.bas.entity;

import cn.action.common.persistence.DataEntity;

public class BomDetail extends DataEntity<BomDetail> {
    //bom详情类
    private Bom bom;//bom外键对象
    private String mType;//材料类型
    private double mNum;//数量
    private String unit;//单位

    public Bom getBom() {
        return bom;
    }

    public void setBom(Bom bom) {
        this.bom = bom;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public double getmNum() {
        return mNum;
    }

    public void setmNum(double mNum) {
        this.mNum = mNum;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


}
