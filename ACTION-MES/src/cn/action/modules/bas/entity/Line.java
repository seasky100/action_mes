package cn.action.modules.bas.entity;

import cn.action.common.persistence.DataEntity;

public class Line extends DataEntity<Line> {
    /*
     *车间实体类
     *
     * */
    private static final long serialVersionUID = 1L;

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getLineNumber() {
        return LineNumber;
    }

    public void setLineNumber(String lineNumber) {
        LineNumber = lineNumber;
    }

    public String getLineMaster() {
        return lineMaster;
    }

    public void setLineMaster(String lineMaster) {
        this.lineMaster = lineMaster;
    }

    public String getLineDescription() {
        return lineDescription;
    }

    public void setLineDescription(String lineDescription) {
        this.lineDescription = lineDescription;
    }

    public WorkShop getWorkShop() {
        return workShop;
    }

    public void setWorkShop(WorkShop workShop) {
        this.workShop = workShop;
    }

    private String lineName; //产线名称
    private String LineNumber;//产线编码
    private String lineMaster;//产线负责人
    private String lineDescription;//产线描述
    private WorkShop workShop;//产线所属车间

}
