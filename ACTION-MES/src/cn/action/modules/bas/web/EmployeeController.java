package cn.action.modules.bas.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.common.web.Response;
import cn.action.common.web.SverResponse;
import cn.action.modules.bas.entity.Employee;
import cn.action.modules.bas.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "${adminPath}/bas/employee")
public class EmployeeController extends BaseController {
    @Autowired
    private EmployeeService employeeService;

    @ModelAttribute("employee")
    public Employee get(@RequestParam(required = false) String id){
        if (StringUtils.isNoneBlank(id)){
            return employeeService.get(id);
        }
        return new Employee();
    }

    //分页按条件查询
    @RequestMapping(value = {"list",""})
    public String list(Employee employee, HttpServletRequest request, HttpServletResponse response, Model model){
        Page<Employee> page=employeeService.findPage(new Page<Employee>(request,response),employee);
        model.addAttribute("page",page);

        String accept = request.getHeader("accept");// 从request中取accept参数（也可用@RequestHeader注解取参数）
        String json = renderString(response, SverResponse.createRespBySuccess(page));// 转换成json字符串
        String url = "modules/bas/employeeList";// 要返回的url
        return Response.CreateResponse(accept, json, url);// 根据accept参数返回json或url字符串
    }

    //保存
    @RequestMapping(value = "save")
    public String save(Employee employee, RedirectAttributes redirectAttributes){
        employeeService.save(employee);
        addMessage(redirectAttributes,"保存员工信息成功!");
        return "redirect:"+adminPath+"/bas/employee";
    }
    //删除
    @RequestMapping(value = "delete")
    public String delete(Employee employee,RedirectAttributes redirectAttributes){
        employeeService.delete(employee);
        this.addMessage(redirectAttributes,"删除员工信息成功！");
        return "redirect:"+adminPath+"/bas/employee";
    }
    @RequestMapping(value = "form")
    public String form(Employee employee, Model model){
        model.addAttribute("employee",employee);
        return "modules/bas/employeeForm";
    }
}
