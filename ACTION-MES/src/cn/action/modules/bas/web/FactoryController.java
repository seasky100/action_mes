package cn.action.modules.bas.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.modules.bas.entity.Enterprise;
import cn.action.modules.bas.entity.Factory;
import cn.action.modules.bas.service.EnterpriseService;
import cn.action.modules.bas.service.FactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "${adminPath}/bas/factory")
public class FactoryController extends BaseController {
    @Autowired
    private FactoryService factoryService;
    @Autowired
    private EnterpriseService enterpriseService;

    //进入controller后先调用该方法
    @ModelAttribute
    public Factory get(@RequestParam(required = false) String id){
        if (StringUtils.isNotBlank(id)){
            return factoryService.get(id);
        }
        return new Factory();
    }
    @RequestMapping(value = {"list",""})
    public String list(Factory factory, HttpServletRequest request, HttpServletResponse response, Model model){
        Page<Factory> page = factoryService.findPage(new Page<Factory>(request,response),factory);
        model.addAttribute("page",page);
        return "modules/bas/factoryList";
    }
    @RequestMapping(value = "save")
    public String save(Factory factory, Model model, RedirectAttributes redirectAttributes){
        factoryService.save(factory);
        addMessage(redirectAttributes,"保存工厂信息成功！");
        return "redirect:"+adminPath+"/bas/factory";
    }
    @RequestMapping(value = "delete")
    public String delete(Factory factory, RedirectAttributes redirectAttributes){
        factoryService.delete(factory);
        addMessage(redirectAttributes,"删除工厂信息成功！");
        return "redirect:"+adminPath+"/bas/factory";
    }
    //跳转到企业添加页面
    @RequestMapping(value = "form")
    public String form(Factory factory,Model model){
        List<Enterprise> list = enterpriseService.findAllList(new Enterprise());
        model.addAttribute("factory",factory);
        model.addAttribute("enterpriseList",list);
        return "modules/bas/factoryForm";
    }
}
