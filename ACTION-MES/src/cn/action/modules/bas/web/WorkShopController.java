package cn.action.modules.bas.web;

import cn.action.common.persistence.Page;
import cn.action.common.utils.StringUtils;
import cn.action.common.web.BaseController;
import cn.action.modules.bas.entity.Factory;
import cn.action.modules.bas.entity.WorkShop;
import cn.action.modules.bas.service.FactoryService;
import cn.action.modules.bas.service.WorkShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "${adminPath}/bas/workShop")
public class WorkShopController extends BaseController {
    @Autowired
    private WorkShopService workShopService;
    @Autowired
    private FactoryService factoryService;
    //进入controller后先调用该方法
    @ModelAttribute
    public WorkShop get(@RequestParam(required = false) String id){
        if (StringUtils.isNotBlank(id)){
            return workShopService.get(id);
        }
        return new WorkShop();
    }
    @RequestMapping(value = {"list",""})
    public String list(WorkShop workshop, HttpServletRequest request, HttpServletResponse response, Model model){
        Page<WorkShop> page = workShopService.findPage(new Page<WorkShop>(request,response),workshop);
        model.addAttribute("page",page);
        return "modules/bas/workShopList";
    }
    @RequestMapping(value = "save")
    public String save(WorkShop workShop, Model model, RedirectAttributes redirectAttributes){
        workShopService.save(workShop);
        addMessage(redirectAttributes,"保存车间信息成功！");
        return "redirect:"+adminPath+"/bas/workShop";
    }
    @RequestMapping(value = "delete")
    public String delete(WorkShop workShop, RedirectAttributes redirectAttributes){
        workShopService.delete(workShop);
        addMessage(redirectAttributes,"删除车间信息成功！");
        return "redirect:"+adminPath+"/bas/workShop";
    }
    //跳转到车间添加页面
    @RequestMapping(value = "form")
    public String form(WorkShop workShop,Model model){
        List<Factory> list = factoryService.findAllList(new Factory());
        model.addAttribute("workShop",workShop);
        model.addAttribute("factoryList",list);
        return "modules/bas/workShopForm";
    }
}
